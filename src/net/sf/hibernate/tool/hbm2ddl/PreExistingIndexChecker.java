package net.sf.hibernate.tool.hbm2ddl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import net.sf.hibernate.mapping.Column;
import net.sf.hibernate.mapping.Index;

public class PreExistingIndexChecker
{
    private static Log log = LogFactory.getLog(PreExistingIndexChecker.class);

    private final TableMetadata tableMetadata;
    private final Map<List<String>, Set<String>> indexColumnNamesToExistingIndexNames;

    public PreExistingIndexChecker(final TableMetadata tableMetadata)
    {
        this.tableMetadata = tableMetadata;
        this.indexColumnNamesToExistingIndexNames = tableMetadata == null ? null : extractIndexColumnNames(tableMetadata);
    }

    /**
     * Examines the meta data on existing indexes against the given column, and logs a warning if any of them are defined
     * against the same column list as the supplied index.
     */
    public void warnIfExistingMatchingIndexesPresent(Index newIndex) {
        if (tableMetadata != null && newIndex != null) {
            final List<String> indexColumns = getIndexColumnNames(newIndex);
            final Set<String> existingMatchingIndexNames = indexColumnNamesToExistingIndexNames.get(indexColumns);
            if (existingMatchingIndexNames != null && !existingMatchingIndexNames.isEmpty())
            {
                log.warn("Pre-existing indexes " + existingMatchingIndexNames + " are present on table '" + tableMetadata.getName() + "' for columns " + getIndexColumnNames(newIndex) + ". New index '" + newIndex.getName() + "' will be a duplicate.");
            }
        }
    }

    private static Map<List<String>, Set<String>> extractIndexColumnNames(final TableMetadata tableMetadata)
    {
        Map<List<String>, Set<String>> columnsMap = new HashMap<List<String>, Set<String>>();
        final Map<String, IndexMetadata> existingIndexes = tableMetadata.getIndexes();
        for (IndexMetadata existingIndex : existingIndexes.values())
        {
            final List<String> columnNames = getIndexColumnNames(existingIndex);
            Set<String> indexNames = columnsMap.get(columnNames);
            if (indexNames == null) {
                indexNames = new HashSet<String>();
                columnsMap.put(columnNames, indexNames);
            }
            indexNames.add(existingIndex.getName());
            columnsMap.put(columnNames, indexNames);
        }
        return columnsMap;
    }

    private static List<String> getIndexColumnNames(IndexMetadata indexMetadata) {
        final List<String> columnNames = new ArrayList<String>();
        for (ColumnMetadata columnMetadata : indexMetadata.getColumns())
        {
            columnNames.add(columnMetadata.getName().toLowerCase());
        }
        return columnNames;
    }

    private static List<String> getIndexColumnNames(Index index) {
        final List<String> columnNames = new ArrayList<String>();
        final Iterator<Column> columnIterator = index.getColumnIterator();
        while (columnIterator.hasNext()) {
            final Column column = columnIterator.next();
            columnNames.add(column.getName().toLowerCase());
        }
        return columnNames;
    }

}