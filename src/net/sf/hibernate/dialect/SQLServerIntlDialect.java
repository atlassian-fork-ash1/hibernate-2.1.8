package net.sf.hibernate.dialect;

import java.sql.Types;

/**
 * A dialect for MS SQL Server 2000, which supports Unicode characters
 */
public class SQLServerIntlDialect extends SQLServerDialect
{
    public SQLServerIntlDialect()
    {
        super();
        registerColumnType( Types.CHAR, "nchar(1)" );
        registerColumnType( Types.VARCHAR, "nvarchar($l)" );
        registerColumnType( Types.CLOB, "ntext" );
    }
}